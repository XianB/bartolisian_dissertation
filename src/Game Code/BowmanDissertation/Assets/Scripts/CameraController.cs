﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static float cps = 6;

    internal bool startMoveIsDoneFlag;

    internal Vector3 cameraStartingPos;
    internal Vector3 cameraCurrentPos;

    internal GameObject targetToLock;
    internal GameObject targetToFollow;

    private GameObject player;
    private GameObject enemy;

    private bool fired = false;
    private bool toPlayer = false;

    void Awake()
    {
        GetComponent<Camera>().orthographicSize = cps;
        cameraStartingPos = new Vector3(0, 0, -20);
        cameraCurrentPos = cameraStartingPos;
        transform.position = cameraStartingPos;
        targetToLock = null;
        targetToFollow = null;
        startMoveIsDoneFlag = false;

        player = GameObject.FindGameObjectWithTag("Player");
        enemy = GameObject.FindGameObjectWithTag("enemy");
    }

    // Start is called before the first frame update
    void Start()
    {
        startMoveIsDoneFlag = true;
    }

    // Update is called once per frame
    void Update()
    {
        handleCps();
    }

    void FixedUpdate()
    {
        
        if(transform.position.y <= 0)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
        /*
        if(transform.position.y > 25)
        {
            transform.position = new Vector3(transform.position.x, 25, transform.position.z);
        }
        */
        if(fired && targetToFollow)
        {
            toPlayer = false;
            StartCoroutine(Wait());
            fired = false;
        }

        if (targetToFollow)
        {
            StartCoroutine(smoothFollow(targetToFollow.transform.position));
            fired = true;
        }
        if(!targetToFollow && toPlayer)
        {
            StartCoroutine(smoothFollow(player.transform.position));
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(5);
        toPlayer = true;
    }

    [Range(0, 0.5f)]
    public float followSpeedDelay = 0.1f;
    private float xVelocity = 0.0f;
    private float yVelocity = 0.0f;
    IEnumerator smoothFollow(Vector3 p)
    {
        float posX = Mathf.SmoothDamp(transform.position.x, p.x, ref xVelocity, followSpeedDelay);
        float posY = Mathf.SmoothDamp(transform.position.y, p.y - 2, ref yVelocity, followSpeedDelay);
        transform.position = new Vector3(posX, posY, transform.position.z);

        cameraCurrentPos = transform.position;

        yield return 0;
    }

    void handleCps()
    {
        if (cps < 5) cps = 5;
        if (cps > 6) cps = 6;

        GetComponent<Camera>().orthographicSize = cps;
    }

    public IEnumerator goToPosition(Vector3 from, Vector3 to, float speed)
    {
        float t = 0;
        while(t < 1)
        {
            t += Time.deltaTime * speed;
            transform.position = new Vector3(Mathf.SmoothStep(from.x, to.x, t), Mathf.SmoothStep(from.y, to.y, t), transform.position.z);

            yield return 0;
        }

        if(t >= 1)
        {
            cameraCurrentPos = transform.position;
        }
    }
}
