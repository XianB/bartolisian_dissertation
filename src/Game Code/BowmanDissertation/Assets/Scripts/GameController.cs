﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameController : MonoBehaviour
{
    public GameObject enemy;
    System.Random r = new System.Random();

    void Awake()
    {
        enemy = Instantiate(enemy, new Vector3(r.Next(120, 180), 0, 0), Quaternion.Euler(0, 180, 0)) as GameObject;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
