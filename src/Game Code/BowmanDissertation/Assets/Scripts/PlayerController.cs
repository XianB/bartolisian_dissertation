﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int baseShootPower = 30;                 //base power. edit with care.
    private int minShootPower = 15;                 //powers lesser than this amount are ignored. (used to cancel shoots)

    [Header("Linked GameObjects")]
    //Reference to game objects (childs and prefabs)
    public GameObject arrow;
    public GameObject playerTurnPivot;
    public GameObject playerShootPosition;
    public Text UiDynamicPower;
    public Text UiDynamicDegree;

    //Hidden gameobjects
    private GameObject cam; //main camera

    //private settings
    private Vector2 icp;                            //initial Click Position
    private Ray inputRay;
    private RaycastHit hitInfo;
    private float inputPosX;
    private float inputPosY;
    private Vector2 inputDirection;
    private float distanceFromFirstClick;
    private float shootPower;
    private float shootDirection;
    private Vector3 shootDirectionVector;

    void Awake()
    {
        icp = new Vector2(0, 0);
        shootDirectionVector = new Vector3(0, 0, 0);
        
        cam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            turnPlayerBody();
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            icp = new Vector2(inputPosX, inputPosY);
            //print("icp: " + icp);
            //print("icp magnitude: " + icp.magnitude);
        }

        //clear the initial Click Position
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {

            //only shoot if there is enough power applied to the shoot
            if (shootPower >= minShootPower)
            {
                shootArrow();
            }
            else
            {
                //reset body rotation
                StartCoroutine(resetBodyRotation());
            }

            //reset variables
            icp = new Vector2(0, 0);
        }
    }

    /// <summary>
    /// When player is aiming, we need to turn the body of the player based on the angle of icp and current input position
    /// </summary>
    void turnPlayerBody()
    {
        //print("Reaching Here");
        inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(inputRay, out hitInfo, 50))
        {
            //print("Reaching Here No2");
            // determine the position on the screen
            inputPosX = this.hitInfo.point.x;
            inputPosY = this.hitInfo.point.y;
            //print("Pos X-Y: " + inputPosX + " / " + inputPosY);

            // set the bow's angle to the arrow
            inputDirection = new Vector2(icp.x - inputPosX, icp.y - inputPosY);
            //print("Dir X-Y: " + inputDirection.x + " / " + inputDirection.y);

            shootDirection = Mathf.Atan2(inputDirection.y, inputDirection.x) * Mathf.Rad2Deg;

            //for an optimal experience, we need to limit the rotation to 0 ~ 90 euler angles.
            //so...
            if (shootDirection > 90)
                shootDirection = 90;
            if (shootDirection < 0)
                shootDirection = 0;

            //apply the rotation
            playerTurnPivot.transform.eulerAngles = new Vector3(0, 0, shootDirection);

            //calculate shoot power
            distanceFromFirstClick = inputDirection.magnitude / 4;
            shootPower = Mathf.Clamp(distanceFromFirstClick, 0, 1) * 100;
            //print ("distanceFromFirstClick: " + distanceFromFirstClick);
            //print("shootPower: " + shootPower);

            //show informations on the UI text elements
            UiDynamicDegree.text = ((int)shootDirection).ToString();
            UiDynamicPower.text = ((int)shootPower).ToString() + "%";
        }
    }

    /// <summary>
    /// Shoot sequence.
    /// For the player controller, we just need to instantiate the arrow object, apply the shoot power to it, and watch is fly.
    /// There is no AI involved with player arrows. It just fly based on the initial power and angle.
    /// </summary>
    void shootArrow()
    {
        GameObject arr = Instantiate(arrow, playerShootPosition.transform.position, Quaternion.Euler(0, 180, shootDirection * -1)) as GameObject;
        arr.name = "PlayerProjectile";
        //arr.GetComponent<MainLauncherController> ().ownerID = 0;

        shootDirectionVector = Vector3.Normalize(inputDirection);
        shootDirectionVector = new Vector3(Mathf.Clamp(shootDirectionVector.x, 0, 1), Mathf.Clamp(shootDirectionVector.y, 0, 1), shootDirectionVector.z);

        arr.GetComponent<MainLauncherController>().playerShootVector = shootDirectionVector * ((shootPower + baseShootPower) / 3);

        //print("shootPower: " + shootPower + " --- " + "shootDirectionVector: " + shootDirectionVector);

        cam.GetComponent<CameraController>().targetToFollow = arr;

        //reset body rotation
        StartCoroutine(resetBodyRotation());
    }


    /// <summary>
    /// tunr player body to default rotation
    /// </summary>
    IEnumerator resetBodyRotation()
    {

        //yield return new WaitForSeconds(1.5f);
        //playerTurnPivot.transform.eulerAngles = new Vector3(0, 0, 0);

        yield return new WaitForSeconds(0.25f);
        float currentRotationAngle = playerTurnPivot.transform.eulerAngles.z;
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * 3;
            playerTurnPivot.transform.rotation = Quaternion.Euler(0, 0, Mathf.SmoothStep(currentRotationAngle, 0, t));
            yield return 0;
        }

    }
}
