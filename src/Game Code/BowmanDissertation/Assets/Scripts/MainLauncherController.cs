﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainLauncherController : MonoBehaviour
{
    internal Vector3 playerShootVector;
    private GameObject enemy;
    private GameObject player;
    private bool stopUpdate;        //we need to stop this weapon after collision


    private GameObject cam; //main camera

    void Awake()
    {
        enemy = GameObject.FindGameObjectWithTag("enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        cam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Start is called before the first frame update
    void Start()
    {
        stopUpdate = false;

        if (playerShootVector.magnitude > 0)
        {
            GetComponent<Rigidbody>().velocity = playerShootVector;
            //GetComponent<Rigidbody>().AddForce(playerShootVector, ForceMode.Impulse);
        }


        Destroy(gameObject, 45);
    }


    //Update is called once per frame
    void Update()
    {
        manageArrowRotation();

        //bug prevention
        if (transform.position.y < -8)
            Destroy(gameObject);
    }
    
    private Vector3 v;      //velocity
    private float zDir;
    private float yDir;
    void manageArrowRotation()
    {
        v = GetComponent<Rigidbody>().velocity;
        if ((v != Vector3.zero) && !stopUpdate)
        {
            zDir = (Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg);
            yDir = Mathf.Atan2(v.z, v.x) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, -yDir, zDir);
        }
    }

    private bool isChecking = false;
    IEnumerator OnCollisionEnter(Collision other)
    {
        //check for collision just once
        if (isChecking)
            yield break;

        isChecking = true;
        string oTag = other.collider.gameObject.tag;    //tag of the object we had collision with

        //Collision with enemy
        if ((oTag == "enemyBody" || oTag == "enemyLeg" || oTag == "enemyHead" || oTag == "floor") && (gameObject.tag == "arrow"))
        {

            //disable the arrow
            stopUpdate = true;
            /*GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().isKinematic = true;

            if (GetComponent<BoxCollider>())
                GetComponent<BoxCollider>().enabled = false;
            
            transform.parent = other.collider.gameObject.transform;*/

            Text Score = GameObject.Find("Score").GetComponent<Text>();

            if (oTag == "enemyBody")
            {
                int sc = int.Parse(Score.text) + 20;
                GameObject.Find("Score").GetComponent<Text>().text = sc.ToString();
                Destroy(gameObject);
            }

            if (oTag == "enemyLeg")
            {
                int sc = int.Parse(Score.text) + 10;
                GameObject.Find("Score").GetComponent<Text>().text = sc.ToString();
                Destroy(gameObject);
            }

            if (oTag == "enemyHead")
            {
                int sc = int.Parse(Score.text) + 50;
                GameObject.Find("Score").GetComponent<Text>().text = sc.ToString();
                Destroy(gameObject);
            }

            if(oTag == "floor")
            {
                int sc = int.Parse(Score.text) + 1;
                GameObject.Find("Score").GetComponent<Text>().text = sc.ToString();
                Destroy(gameObject);
            }
        }
    }
}
